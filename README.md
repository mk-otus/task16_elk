# task16_elk

Домашнее задание
Настраиваем центральный сервер для сбора логов
в вагранте поднимаем 2 машины web и log
на web поднимаем nginx
на log настраиваем центральный лог сервер на любой системе на выбор
- journald
- rsyslog
- elk
настраиваем аудит следящий за изменением конфигов нжинкса
```
все критичные логи с web должны собираться и локально и удаленно
все логи с nginx должны уходить на удаленный сервер (локально только критичные)
логи аудита должны также уходить на удаленную систему
```
--------------------------------------------------------------------------------
В этот раз сделаем инфраструктуру в DigitalOcean, тем более одна виртуалка с логами nginx уже там есть
В ELK будут слаться все логи - nginx auditd system


```
На ELK хосте 

yum update
yum -y install java-11-openjdk-devel perl-Digest-SHA wget curl iptables-services unzip epel-release vim
yum -y install certbot
yum -y install python2-certbot-dns-digitalocean
 
# https://certbot-dns-digitalocean.readthedocs.io/en/stable/
# Сделать токен в "API" (панель управления DO) с именем dns_digitalocean_token и DNS-записи в DO.
# Сделать credentials-файл /root/digitalocean.ini с таким содержанием:
 
# DigitalOcean API credentials used by Certbot
dns_digitalocean_token = 0000111122223333444455556666777788889999aaaabbbbccccddddeeeeffff
 
# Выставить права 600 для файла digitalocean.ini
 
chmod 600 digitalocean.ini
 
# Выполнить команду:
 
certbot certonly \
  --dns-digitalocean \
  --dns-digitalocean-credentials /root/digitalocean.ini \
  -d *.f0d51be7fb.com
 
# Ввести email и согласиться с правилами. Сертификаты будут находиться в /etc/letsencrypt/live/f0d51be7fb.com/
 
# Установка Elasticsearch:
 
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.4.0-x86_64.rpm
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.4.0-x86_64.rpm.sha512
shasum -a 512 -c elasticsearch-7.4.0-x86_64.rpm.sha512
sudo rpm --install elasticsearch-7.4.0-x86_64.rpm
 
# Конфиг Elasticsearch:
 
cat <<EOF > /etc/elasticsearch/elasticsearch.yml
cluster.name: test_cluster
node.name: $HOSTNAME
 
path.data: /var/lib/elasticsearch
path.logs: /var/log/elasticsearch
 
discovery.type: single-node
EOF
 
# Установка Kibana:
 
wget https://artifacts.elastic.co/downloads/kibana/kibana-7.4.0-x86_64.rpm
shasum -a 512 kibana-7.4.0-x86_64.rpm
sudo rpm --install kibana-7.4.0-x86_64.rpm
 
# Конфиг Kibana:
 
cat <<EOF > /etc/kibana/kibana.yml
server.host: "0.0.0.0"
 
elasticsearch.hosts: ["http://localhost:9200"]
EOF
 
# Запустим Elasticsearch и Kibana:
 
systemctl start elasticsearch.service && systemctl enable elasticsearch.service
systemctl start kibana.service && systemctl enable kibana.service
 
# Проверим в консоли:
 
curl -X GET "localhost:9200/_cluster/health?pretty"
 
# И в браузере:
 
http://<myip>:5601
 
# Сконфигурируем security.
# Создадим папку /etc/elasticsearch/certs
 
mkdir /etc/elasticsearch/certs
 
# Скопируем в папку /etc/elasticsearch/certs сертификаты из /etc/letsencrypt/archive/f0d51be7fb.com
 
cp /etc/letsencrypt/archive/f0d51be7fb.com/* /etc/elasticsearch/certs/
 
# И выставим права 770
 
chmod -R 770 /etc/elasticsearch/certs/
 
# Поправим конфиг /etc/elasticsearch/elasticsearch.yml с учетом использования сертификатов:
 
cluster.name: test_cluster
node.name: elasticsearch-test
 
path.data: /var/lib/elasticsearch
path.logs: /var/log/elasticsearch
 
#network.host: elk.f0d51be7fb.com
network.host: 0.0.0.0
 
discovery.type: single-node
 
xpack.security.enabled: true
xpack.security.http.ssl.enabled: true
xpack.security.transport.ssl.enabled: true
xpack.security.http.ssl.key: certs/privkey1.pem
xpack.security.http.ssl.certificate: certs/fullchain1.pem
xpack.security.transport.ssl.key: certs/privkey1.pem
xpack.security.transport.ssl.certificate: certs/fullchain1.pem
 
# Перезапустим Elasticsearch:
 
systemctl restart elasticsearch.service
 
# Сгенерируем пароли автоматически:
 
/usr/share/elasticsearch/bin/elasticsearch-setup-passwords auto -u "https://elk.f0d51be7fb.com:9200"
 
# Увидим такой вывод:
 
Initiating the setup of passwords for reserved users elastic,apm_system,kibana,logstash_system,beats_system,remote_monitoring_user.
The passwords will be randomly generated and printed to the console.
Please confirm that you would like to continue [y/N]y
 
 
Changed password for user apm_system
PASSWORD apm_system = hAS4k1fi6AtLW9fJELys
 
Changed password for user kibana
PASSWORD kibana = 2aPgLsggZaA7lhB4mnES
 
Changed password for user logstash_system
PASSWORD logstash_system = ypdWPXDNvIF1IOxg3X9O
 
Changed password for user beats_system
PASSWORD beats_system = GAajNoEw6YOMtZrhWyci
 
Changed password for user remote_monitoring_user
PASSWORD remote_monitoring_user = nKTsQ1IdkVIpUagTFcyc
 
Changed password for user elastic
PASSWORD elastic = xMpvuOBfqOYCypMTK45A
 
# Проверим:
 
curl --cacert /etc/elasticsearch/certs/fullchain1.pem -u elastic 'https://elk.f0d51be7fb.com:9200/_cat/nodes?v'
 
# введем пароль:
 
Enter host password for user 'elastic':
 
# увидим
 
ip            heap.percent ram.percent cpu load_1m load_5m load_15m node.role master name
68.183.10.202           20          95   4    0.09    0.17     0.17 dim       *      elasticsearch-test
 
# создадим папку для сертификатов Kibana, скопируем сертификаты, выставим для них права и владельца:
 
mkdir /etc/kibana/certs
 
cp /etc/letsencrypt/archive/f0d51be7fb.com/* /etc/kibana/certs
 
chmod -R 770 /etc/kibana/certs && chown -R kibana. /etc/kibana/certs
 
# поправим конфиг Kibana /etc/kibana/kibana.yml
 
server.name: elasticsearch-test
#server.host: "elk.f0d51be7fb.com"
server.host: 0.0.0.0
 
server.ssl.enabled: true
server.ssl.certificate: /etc/kibana/certs/fullchain1.pem
server.ssl.key: /etc/kibana/certs/privkey1.pem
elasticsearch.hosts: ["https://elk.f0d51be7fb.com:9200"]
elasticsearch.username: "kibana"
elasticsearch.password: "2aPgLsggZaA7lhB4mnES"
 
# перезапустим Kibana
 
systemctl restart kibana.service
 
# проверим, залогинимся под пользователем elastic
 
https://elk.f0d51be7fb.com:5601
 
# Установка Logstash
 
wget https://artifacts.elastic.co/downloads/logstash/logstash-7.4.0.rpm
rpm --install logstash-7.4.0.rpm
 
# создадим папку для сертификатов Logstash, скопируем сертификаты:
 
mkdir /etc/logstash/certs
 
cp /etc/letsencrypt/archive/f0d51be7fb.com/* /etc/logstash/certs
 
# Поправим конфиг Logstash /etc/logstash/logstash.yml
 
node.name: elasticsearch-test
 
path.data: /var/lib/logstash
path.logs: /var/log/logstash
 
# X-Pack Monitoring
# https://www.elastic.co/guide/en/logstash/current/monitoring-logstash.html
xpack.monitoring.enabled: true
xpack.monitoring.elasticsearch.username: logstash_system
xpack.monitoring.elasticsearch.password: 'ypdWPXDNvIF1IOxg3X9O'
xpack.monitoring.elasticsearch.hosts: ["https://elk.f0d51be7fb.com:9200"]
xpack.monitoring.elasticsearch.ssl.certificate_authority: "/etc/logstash/certs/fullchain1.pem"
xpack.monitoring.elasticsearch.ssl.verification_mode: certificate
xpack.monitoring.elasticsearch.sniffing: false
xpack.monitoring.collection.interval: 10s
xpack.monitoring.collection.pipeline.details.enabled: true
 
# Запустим Logstash
 
systemctl start logstash.service
 
# Создадим роль logstash_writer через Console UI
 
POST /_security/role/logstash_writer
{
  "cluster": [
    "manage_index_templates",
    "monitor",
    "manage_ilm",
    "manage_ml",
    "manage_pipeline",
    "manage_ingest_pipelines"    
  ],
  "indices": [
    {
      "names": [
        "logstash-*"
      ],
      "privileges": [
        "write",
        "create_index",
        "manage",
        "manage_ilm"
      ]
    }
  ]
}
 
# и пользователя logstash_internal
 
POST /_security/user/logstash_internal
{
  "password": "1095745h",
  "roles": [
    "logstash_writer"
  ]
}
```
Настройка хоста с nginx будем намного проще, так как мои логи стандартные предобработку в logstash могу не делать, а сразу слать в elasticsearch

```
curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.4.0-x86_64.rpm
sudo rpm -vi filebeat-7.4.0-x86_64.rpm
cp /etc/letsencrypt/archive/f0d51be7fb.com/* /etc/filebeat/certs/


Добавим в конфиг /etc/filebeat/fileveat.yml строчки
filebeat.config.modules:
  path: ${path.config}/modules.d/*.yml
  reload.enabled: false
  #reload.period: 10s

output.elasticsearch:
  hosts: ["https://elk.f0d51be7fb.com:9200"]
  ssl.certificate_authorities: ["/etc/filebeat/certs/fullchain1.pem"]
  #ssl.certificate: "/etc/filebeat/certs/privkey1.pem"
  #ssl.key: "/etc/filebeat/certs/sergei-latitude.key"
  username: "filebeat_internal"
  password: "passwd"
  ilm.enabled: true
  setup.template.overwrite: true

setup.kibana:
  host: "https://elk.f0d51be7fb.com:5601"
  ssl.enabled: true
  ssl.certificate_authorities: ["/etc/filebeat/certs/fullchain1.pem"]
  #ssl.certificate: "/etc/filebeat/certs/privkey1.pem"
  #ssl.key: "/etc/filebeat/certs/sergei-latitude.key"

setup.dashboards:
  enabled: true

processors:
  - add_host_metadata: ~
  - add_cloud_metadata: ~



filebeat modules enable nginx auditd system
filebeat setup -e
systemctl enable filebeat
systemctl start filebeat 

Теперь можно поделать скриншоты, все логи прилетают


```






